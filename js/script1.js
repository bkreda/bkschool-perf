//GLOBAL VARIABLE :
var contactName;

var constant = {
	status : {
		CONNECTED 	 : '200',
		DISCONNECTED : '480',
		UNAUTHORIZED : '401',
		NOT_FOUND 	 : '404',
		PROXY_AUTHENTICATION_REQUIRED : '407',
		BAD_GATEWAY	 : '502'
	}
};

	
var contactMenuContentUrl = "contacts-menu-content.php";

var Tv = Tv ||{};
Tv.SendSms = Tv.SendSms ||{};
Tv.SendSms.Cs = Tv.SendSms.Cs ||{};

Tv.SendSms.Cs.CM_SEARCH_BAR_HEIGHT = 26; /* Sous firefox 26 c'est très bien. */ //31;




function buildContactMenu(){
	
	//dataType : "text", Si mis dans ajax : pose des problèmes d'encoding indéchiffrables :'(
	//http://forum.springsource.org/showthread.php?p=321061#post321061
	$.ajax({
		url : contactMenuContentUrl,
		dataType:"json",
		success : function(data) {
			
			Tv.Contact.contactDATA = data;	
			
			console.time("setContactMenuContentTimer");
			setContactMenuContent();
			console.timeEnd("setContactMenuContentTimer");
			
		}
	});
};


function setContactMenuContent(){

	var menuContent = buildMenuContent(Tv.Contact.contactDATA);

	$("#nav").html(menuContent.ul);

	if(menuContent.backText){
		$('#nav > ul').ddMenu({rootTitle: menuContent.rootTitle, 
							   duration: 250, 
							   backText:menuContent.backText,
							   startTypingText:startTyping}); //defined in send-sms.jsp
							   
	}else{
		$('#nav > ul').ddMenu({rootTitle: menuContent.rootTitle, duration: 250});
	}
	

	//Éviter le display none sur le menu avant sa fabrication.
	$('.inner').hide();//cacher le ddMenu

	$("#nav").css("text-indent",0);
	$("#nav").css("position","static");
	
	//Moving elements : http://www.elated.com/articles/jquery-removing-replacing-moving-elements/
//	$(".inner").append( $("#nav1") );
	$("#nav").appendTo( ".inner" );
	
	//Change the loading img to a button and make it clickable
	$('#slidebottom img').attr("src", "images/android_head2.png").addClass("contact-button");
	
	//Add behaviour to contact button
	$('#slidebottom img').click(function() {
		
		var h;

		if(! $(this).hasClass("cm-open")){ //nécessaire pour le début
			h = Tv.SendSms.Cs.CM_SEARCH_BAR_HEIGHT + "px";//25	
			$('.bdc-dd-menu').css("height",h);
		}
		
		if (! $(this).hasClass("cm-open")) {
			openContactMenu();			
		}else{
			closeContactMenu();
		}
		
		
	});
	
	//Append clicked phone number to destination input, setup error page 
	if(menuContent.contactErrorPage)
	{
		addErrorBehavior(menuContent.contactErrorPage);	
	}
	else{
		addPhoneBehavior();	
	}
};

/**
 * D'abord ouvre la zone de recherche, ensuite ouvre la liste des contact (bdc-dd-menu)
 * http://www.learningjquery.com/2009/02/slide-elements-in-different-directions
 */
function openContactMenu(){
	$('#slidebottom img').next().animate({width: 'toggle'}, 
								   {
								    duration: 300,
								    complete: function() {
										        	$('.bdc-dd-menu').animate({height:411},{duration: 200}); //height: 171
													$('#slidebottom img').addClass("cm-open");
										       }
								   });
}


/**
 * D'abord ferme la liste des contacts (bdc-dd-menu) ensuite ferme la zone de recherche.
 */
function closeContactMenu(){
	$('.bdc-dd-menu').animate(
		{height: Tv.SendSms.Cs.CM_SEARCH_BAR_HEIGHT },
		{
			duration: 300,
			complete: function(){
				$('#slidebottom img').next().animate({width: 'toggle'});
			}
		}
	);
	$('#slidebottom img').removeClass("cm-open");
}


function addPhoneBehavior(){
	
	//Get the contact name
	$("span.bdc-dd-text").click(function(e){
		//$("#share_menu_fieldset input[name=destination]").val($(this).attr("href"));
		contactName = $(this).html();
		
//		e.preventDefault();
	});
	
	
	//Get the contact phone number
	$(".inner a").click(function(e){
		//$("#share_menu_fieldset input[name=destination]").val($(this).attr("href"));
		fillDestination($(this).attr("href"));

		closeContactMenu();			
			
		e.preventDefault();
	});
}

/**
 * @param phoneNumber
 */
function fillDestination(phoneNumber){
	 $("#destinations").trigger("addItem",[{"title": contactName +" (" + phoneNumber + ")", "value": phoneNumber}]);
}

function addErrorBehavior(page){
	$("#nav a").click(function(e){
		$('.inner').hide();
		e.preventDefault();
		//window.open(page,'_newtab');
		window.location = page;
	});
}


function buildMenuContent(data){
	var menuContent = new Object();
	
	if(!data){//NO DATA
		handleNoData(menuContent);
    	return menuContent;
	}
	
		
	if(data.length == 1){//Looking for errors
		
		var nick = data[0].nick;
		if (nick == "error"){
			handleErrorData(data, menuContent);
	    	return menuContent;
		}
	}

	handleContactData(data, menuContent);
	return menuContent;
}


//some innerHTML
function handleContactData(data, menuContent){
//root ul
	var ul = document.createElement('ul');
	for(var k = 0 ; k < data.length; k++){
		var li = document.createElement('li');
		var span = '<span>' + data[k].nick + '</span>';
		
		var ul_c = document.createElement('ul');
		for(var j=0; j< data[k].phones.length ; j++)
		{
			var li_ph = document.createElement('li');
			var a = "<a + href='" + data[k].phones[j] + "'>" + data[k].phones[j] + "</a>";
			
			li_ph.innerHTML = a;
			ul_c.appendChild(li_ph);
		}
		
		li.innerHTML = span;
		li.appendChild(ul_c);
		ul.appendChild(li);
	}
	
	
	menuContent.ul = $(ul);
	
	menuContent.rootTitle = "Choose a Contact"; //Not used
	menuContent.backText  = backText;	
}

//some appendChild
function handleContactDataNoJquery(data, menuContent){
//root ul
	var ul = document.createElement('ul');
	for(var k = 0 ; k < data.length; k++){
		//var li = $(document.createElement('li'));
		var li = document.createElement('li');
		var span = document.createElement('span');
		span.innerHTML = data[k].nick;
		
		var ul_c = document.createElement('ul');
		for(var j=0; j< data[k].phones.length ; j++)
		{
			var li_ph = document.createElement('li');
			var a = document.createElement('a');
			a.innerHTML = data[k].phones[j];
			a.setAttribute('href', data[k].phones[j]);
			
			li_ph.appendChild(a);
			ul_c.appendChild(li_ph);
		}
		
		li.appendChild(span);
		li.appendChild(ul_c);
		ul.appendChild(li);
	}
	
	
	menuContent.ul = $(ul);
	
	menuContent.rootTitle = "Choose a Contact"; //Not used
	menuContent.backText  = backText;	
}
	
function oldHandleContactData(data, menuContent){
//root ul
	var ul = $(document.createElement('ul'));
	for(var k = 0 ; k < data.length; k++){
		//var li = $(document.createElement('li'));
		var li = $('<li>');//Je croix que c'est plus lent que la création en js normal
		var span = $(document.createElement('span'));
		span.html(data[k].nick);
		
		var ul_c = $(document.createElement('ul'));
		for(var j=0; j< data[k].phones.length ; j++)
		{
			var li_ph = $(document.createElement('li'));
			var a = $(document.createElement('a'));
			a.html(data[k].phones[j]);
			a.attr("href",data[k].phones[j]);
			
			li_ph.html(a);
			ul_c.append(li_ph);
		}
		
		li.append(span);
		li.append(ul_c);
		ul.append(li);
	}
	
	
	menuContent.ul = ul;
	
	menuContent.rootTitle = "Choose a Contact"; //Not used
	menuContent.backText  = backText;	
}


function handleErrorData(data, menuContent){
	var errorText='';
	var a = $(document.createElement('a'));
	var errorCode = data[0].phones[0];

	
	switch (errorCode){
		case constant.status.PROXY_AUTHENTICATION_REQUIRED :
			a.html(goToProfilePage);
			errorText = gmail407;
			break;
			
		case constant.status.UNAUTHORIZED :
			//UNAUTHORIZED gmail
			a.html(goToProfilePage);
			errorText = gmail401;
			break;
			
		case constant.status.NOT_FOUND :
			//UNAUTHORIZED smshare
			a.html('');
			errorText = reconnectionRequired;
			break;
			
		case constant.status.BAD_GATEWAY :
			a.html('');
			errorText = gmail502;
			break;
	}
	
	a.css("white-space", "normal");    //on firefox long text will not be visible.
									   //let's remove white-space: wrap; when handling error.
	
	a.html(errorText + "<br><br>" + a.html());
	
	var li = $(document.createElement('li'));
	if(a){
		li.html(a);
	}

	var ul = $(document.createElement('ul'));
	ul.append(li);
	
	menuContent.ul = ul;
	menuContent.rootTitle = errorText;
	menuContent.contactErrorPage = profileUrl;	
}


function handleNoData(menuContent){
	errorText = noContactData;
	var li_ph = $(document.createElement('li'));
	var a = $(document.createElement('a'));
	a.html(errorText+ "<br><br>" +leaveUsMsg);
	li_ph.html(a);
	var ul = $(document.createElement('ul'));
	ul.append(li_ph);
	
	menuContent.ul = ul;
	menuContent.rootTitle = errorText;
	menuContent.contactErrorPage = contactUrl;
}


$(function() { // JQUERY ON LOAD
	
	buildContactMenu();
	//Éviter le display none sur le menu avant sa fabrication. 
	//Sinon les propriétés css seront effacées et le dd menu verra height == 0 sur le deuxième menu
	//$("#fieldset1").hide();

});// Fin de jquery onLoad