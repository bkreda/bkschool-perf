$(function() {

	//tooltip : ce bloc suffit au lieu des deux blocs en bas
	//il suffit de mettre class="img4validation"
	// select all desired input fields and attach tooltips to them
	
	$("#img4msg, .img4validation").tooltip({

		// place tooltip on the right edge
		//position: "center right",
		position: "center right",

		// a little tweaking of the position : vertical , horizontal
		offset: [0, 10],

		// use the built-in fadeIn/fadeOut effect
		effect: "fade",

		// custom opacity setting
		opacity: 0.7,

		// use this single tooltip element
		tip: '.tooltip'

	});
	
});

var Tv = Tv || {};

//common isosp
Tv.log = {
	init: function(){
		if(!window.console){
			window.console = {};
		}
		if(!console.log){
			console.log = function(){};
		}		
	}	
};
//Initialisation
Tv.log.init();
	
Tv.Common = Tv.Common || {};

/**
 * Make sure to set the doc encoding to UTF8!
 */
Tv.Common.accentsTidy = function(s){
	//var reg = /[èéêë]/g;
	var r=s.toLowerCase();
//    r = r.replace(new RegExp("\\s", 'g'),"");
    r = r.replace(new RegExp("[àáâãäå]", 'g'),"a");
    r = r.replace(new RegExp("æ", 'g'),"ae");
    r = r.replace(new RegExp("ç", 'g'),"c");
    r = r.replace(new RegExp("[èéêë]", 'g'),"e");
    r = r.replace(new RegExp("[ìíîï]", 'g'),"i");
    r = r.replace(new RegExp("ñ", 'g'),"n");                            
    r = r.replace(new RegExp("[òóôõö]", 'g'),"o");
    r = r.replace(new RegExp("œ", 'g'),"oe");
    r = r.replace(new RegExp("[ùúûü]", 'g'),"u");
    r = r.replace(new RegExp("[ýÿ]", 'g'),"y");
//    r = r.replace(new RegExp("\\W", 'g'),"");
    return r;
};


Tv.Common.saveCookie = function(name, value){
	var maxAge = 60*60*24*30; //expire after one month. //Not supported by IE6,7,8, 9? ...
	var d = new Date();
	d.setTime(d.getTime() + maxAge*1000); // in milliseconds
	document.cookie = name+'='+value+';path=/;expires='+d.toGMTString()+';' ;//max-age='+maxAge+";";
};

/**
 * http://techpatterns.com/downloads/javascript_cookies.php
 * @param check_name
 * @returns
 */
function getCookie( check_name ) {
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f

	for (var i = 0; i < a_all_cookies.length; i++ )
	{
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );


		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		// if the extracted name matches passed check_name
		if ( cookie_name == check_name )
		{
			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( a_temp_cookie.length > 1 )
			{
				cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// note that in cases where cookie is initialized but no value, null is returned
			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if ( !b_cookie_found )
	{
		return null;
	}
}



/**
 * Won't work, replace cookie value instead
 * @deprecated Don't use !
 * @param name
 * @param path
 * @param domain
 */
function deleteCookie( name, path, domain ) {
	//console.log("deleting a cookie")
	if ( getCookie( name ) ) {
		//console.log("cookie was found")
		document.cookie = name + "=" +
		( ( path ) ? ";path=" + path : "") +
		( ( domain ) ? ";domain=" + domain : "" ) +
		";expires="+new Date().toGMTString();
		
		//";expires=Thu, 01-Jan-1970 00:00:01 GMT;max-age=0";
	}else {
		//console.log("cookie was not found")
	}
	
}